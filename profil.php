<?php
include('function.php');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
if (isset($_SESSION['username']) && isset($_SESSION['psw'])) {
	$user = checkAdmin($_SESSION['username']);
	$archives = archives($_SESSION['username']);
	$lastconnexion = lastConnexion($_SESSION['username']);
?>
	<!DOCTYPE html>
	<html lang="en">

	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="style.css" rel="stylesheet">
		<link rel="preconnect" href="https://fonts.googleapis.com">
		<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
		<link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet">
		<!-- CSS only -->
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
		<!-- JavaScript Bundle with Popper -->
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
		<title>Calculateur IMC</title>
	</head>

	<body>

		<header>
			<div class="headerpicture">
				<img src="pictures/logo.png" alt="logo">
			</div>
			<nav class="navdesktop">
				<ul>
					<li><a class="btnfillred" href="#profil">Mon profil</a></li>
					<li><a class="btnfillred" href="#imc">Calculer mon IMC</a></li>
					<li><a class="btnfillred" href="#metabolisme">Besoins caloriques</a></li>
					<li><a class="btnfillred" href="#histo">Archives</a></li>
					<li><a class="btnfillred" href="tel:0139380101">Contactez-nous</a></li>
					<li><a class="btnfillred" href="logout.php">Deconnectez-vous</a></li>
				</ul>
			</nav>
			<div id="mySidenav" class="sidenav">
				<a id="closeBtn" class="close">×</a>
				<ul>
					<li class="btnfillred"><a href="#profil">Mon profil</a></li>
					<li class="btnfillred"><a href="#imc">Calculer mon IMC</a></li>
					<li class="btnfillred"><a href="#metabolisme">Besoins caloriques</a></li>
					<li class="btnfillred"><a href="#histo">Archives</a></li>
					<li class="btnfillred"><a href="tel:0139380101">Contactez-nous</a></li>
					<li class="btnfillred"><a href="logout.php">Deconnectez-vous</a></li>
				</ul>
			</div>
			<a href="#" id="openBtn">
				<span class="burger-icon">
					<span></span>
					<span></span>
					<span></span>
				</span>
			</a>

		</header>
		<section>
			<div id="profil" class="renseignements">
				<div class='left'>
					<h3>Mes données personnelles:</h3>
					<p>Nom: <?= $user['name'] ?></p>
					<p>Prénom: <?= $user['firstname'] ?> </p>
					<p>Adresse email: <br><?= $_SESSION['username'] ?></p>
					<p>Inscrit depuis le: <?= $user['date'] ?></p>
				</div>
				<div class="right">
					<h3>Vos dernières données enregistrées:</h3>
					<?php if (!empty($lastconnexion)) { ?>
						<p>Taille: <?= $lastconnexion['taille'] / 100 ?> m</p>
						<p>Poids actuel: <?= $lastconnexion['poids'] ?> kg </p> <?php } ?>
					<div class="renseignementsbtn">
						<form method="post">
							<input type="submit" name="modif" class="btnred" value="modifier vos données">
							<input type="submit" name="delete" class="btnred" value="supprimer votre compte">
						</form>
						<?php
						if (isset($_POST['delete'])) {
							deleteAccount($_SESSION['username']);
							header('location:account.php');
						}
						if (isset($_POST['modif'])) {
						?>
							<div class="modiform">
								<form class="column" id="centrer" action="profil.php#renseignements" method="post">
									<input value='<?= $user['id'] ?>' class="form-control" type="hidden" name="id" required>
									<p>Votre email :</p> <input value='<?= $_SESSION['username'] ?>' class="form-control" type="text" name="mail" required>
									<p>Votre mot de passe :</p> <input value='<?= $user['password'] ?>' class="form-control" type="password" name="psw" required>
									<p>Votre nom :</p> <input value='<?= $user['name'] ?>' class="form-control" type="text" name="nom" required>
									<p>Votre prénom :</p> <input value='<?= $user['firstname'] ?>' class="form-control" type="text" name="prenom" required>
									<p>Votre sexe :</p> <input value='<?= $user['sexe'] ?>' class="form-control" type="text" name="sex" required>
									<p>Votre date de naissance:</p> <input value='<?= $user['ddn'] ?>' class="form-control" type="date" name="date" required>
									<p><input class="btnred" name="submit" type="submit" value="valider modifications"></p>
								</form>
							</div>
						<?php

						}
						if (isset($_POST['id']) && isset($_POST['mail']) && isset($_POST['psw']) && isset($_POST['nom']) && isset($_POST['prenom']) && isset($_POST['sex']) && isset($_POST['date'])) {
							modifInformations($_POST['nom'], $_POST['prenom'], $_POST['sex'], $_POST['mail'], $_POST['psw'], $_POST['date'], $_POST['id']);
							changeMailImc($_POST['mail'], intval($_POST['id']));
							changeMailcalories($_POST['mail'], intval($_POST['id']));
							session_destroy();
							session_start();
							$_SESSION['username'] = $_POST['mail'];
							$_SESSION['psw'] = $_POST['psw'];
							header('location:profil.php');
						}
						?>
					</div>
				</div>
			</div>

			<div class="imgimc">
				<img src="pictures/imc.webp" alt="bonhommes">
			</div>

			<div id="imc" class="imc">
				<h2>CALCULER VOTRE INDICE DE MASSE CORPORELLE
				</h2>
				<form name="calculePoid" method="post" action="profil.php#imc">
					<div>
						<label for="taille">Poids(en Kg)</label>
						<input class="form-control" type="number" step="1" id="poids" name='poids'>
					</div>
					<div>
						<label for="poids">Taille (en cm)</label>
						<input class="form-control" type="number" id="taille" name="taille" required>
					</div>

					<input class="btnfillred" type="submit" value="Calculer" name="submit">
				</form>
				<?php
				if (isset($_POST['taille']) && isset($_POST['poids'])) {
					$taille = $_POST['taille'];
					$poids = $_POST['poids'];
					$timeimc = time();
					$date = date("d.m.y");
					$imc = ($poids) / ($taille / 100 * $taille / 100);

					if ($imc < 16) {
						$resultimc = "maigreur extrême";
					} else if ($imc < 18.5) {
						$resultimc = "maigreur";
					} else if ($imc < 24.9) {
						$resultimc = "poids normal";
					} else if ($imc < 29.9) {
						$resultimc = "en surpoids";
					} else if ($imc < 34.9) {
						$resultimc = "obésité léger";
					} else if ($imc < 39.9) {
						$resultimc = "obésité";
					} else if ($imc > 40) {
						$resultimc = "obésité morbide";
					}

					addIMC($user['id'], $_SESSION['username'], $poids, $taille, $resultimc, intval($imc), $date, $timeimc);
				?>
					<div class="result">
						<h2>Votre iMC est de : <?= intval($imc) ?> </h2>
						<h2>Votre corpulence : <?= $resultimc ?></h2>
					</div>
				<?php
				};	 ?>
				<div class='desk'>
					<img src="pictures/plat.webp" alt="plat">
				</div>
			</div>
			<div id="metabolisme" class="metabolisme">
				<h2>CALCULEZ VOS BESOINS CALORIQUES</h2>
				<form name="metabolisme" method="post" action="profil.php#metabolisme">
					<div class="form-group">
						<label for="pds">Poids(en Kg)</label>
						<input class="form-control" type="number" id="pds" name='poidsbis' required>
					</div>
					<div class="form-group">
						<label for="height">Taille (en cm)</label>
						<input class="form-control" type="number" name="taillebis" id="height" required>
					</div>


					<div>
						<label>Votre niveau d'activité ?</label>

						<select class="form-control" id="exampleFormControlSelect1" name="actif" required>
							<option value="">choisissez votre cas </option>
							<option value="sédentaire"> sédentaire</option>
							<option value="peu">peu </option>
							<option value="souvent">souvent</option>
							<option value="quotidiennement">quotidiennement</option>

						</select>
					</div>
					<div>

					</div>
					<input type="submit" class="btnfillred" value="Calculer" name="submit">
				</form>
				<?php
				if (isset($_POST['taillebis']) && isset($_POST['poidsbis']) && isset($_POST['actif'])) {
					$taille = $_POST['taillebis'];
					$poids = $_POST['poidsbis'];
					$actif = $_POST['actif'];
					$timemetab = time();
					$sex = $user['sexe'];
					$timemetabolism = time();
					$dateNaissance = $user['ddn'];
					$aujourdhui = date("Y-m-d");
					$diff = date_diff(date_create($dateNaissance), date_create($aujourdhui));
					$age = $diff->format('%y');
					$metawoman = (9.740 * $poids) + (172.9 * ($taille / 100)) - (4.737 * $age) + 667.051;
					$metaman = (13.707 * $poids) + (492.3 * ($taille / 100)) - (6.673 * $age) + 77.607;
					if ($actif === 'sédentaire') {
						if ($sex === 'femme') {

							$metawoman = $metawoman * 1.2;
							addmetabolisme($user['id'], $_SESSION['username'], $taille, $poids, $sex, $age, $metawoman, $aujourdhui, $timemetab);
				?><div class="result">
								<p>besoins caloriques journaliers: <?= intval($metawoman)  ?></p>
							</div>
						<?php

						} else {

							$metaman = $metaman * 1.2;
							addmetabolisme($user['id'], $_SESSION['username'], $taille, $poids, $sex, $age, $metaman, $aujourdhui, $timemetab);
						?><div class="result">
								<p>besoins caloriques journaliers: <?= $metaman ?></p>
							</div>
						<?php

						}
					}
					if ($actif === 'peu') {
						if ($sex === 'femme') {
							$metawoman = $metawoman * 1.375;
							addmetabolisme($user['id'], $_SESSION['username'], $taille, $poids, $sex, $age, $metawoman, $aujourdhui, $timemetab);
						?>
							<div class="result">
								<p>besoins caloriques journaliers: <?= intval($metawoman) ?></p>
							</div>
						<?php
						} else {
							$metaman = $metaman * 1.375;
							addmetabolisme($user['id'], $_SESSION['username'], $taille, $poids, $sex, $age, $metaman, $aujourdhui, $timemetab);
						?>
							<div class="result">
								<p>besoins caloriques journaliers: <?= intval($metaman) ?></p>
							</div>
						<?php
						}
					}
					if ($actif === 'souvent') {
						if ($sex === 'femme') {
							$metawoman = $metawoman * 1.55;
							addmetabolisme($user['id'], $_SESSION['username'], $taille, $poids, $sex, $age, $metawoman, $aujourdhui, $timemetab);
						?>
							<div class="result">
								<p>besoins caloriques journaliers: <?= intval($metawoman) ?></p>
							</div>
						<?php
						} else {
							$metaman = $metaman * 1.55;
							addmetabolisme($user['id'], $_SESSION['username'], $taille, $poids, $sex, $age, $metaman, $aujourdhui, $timemetab);
						?>
							<div class="result">
								<p>besoins caloriques journaliersf: <?= intval($metaman) ?></p>
							</div>
						<?php
						}
					}
					if ($actif === 'quotidiennement') {
						if ($sex === 'femme') {
							$metawoman = $metawoman * 1.725;
							addmetabolisme($user['id'], $_SESSION['username'], $taille, $poids, $sex, $age, $metawoman, $aujourdhui, $timemetab);
						?>
							<div class="result">
								<p>besoins caloriques journaliers: <?= intval($metawoman) ?></p>
							</div>
						<?php
						} else {
							$metaman = $metaman * 1.725;
							addmetabolisme($user['id'], $_SESSION['username'], $taille, $poids, $sex, $age, $metaman, $aujourdhui, $timemetab);
						?>
							<div class="result">
								<p>besoins caloriques journaliers: <?= intval($metaman) ?></p>
							</div>
				<?php
						}
					}
				};
				?>
			</div>
			<?PHP
			unset($_POST);

			?>
			<div id="histo" class="historique">

				<h2>HISTORIQUE PERSONNEL</h2>
				<?php
				foreach ($archives as $archive) { ?>

					<div class="card" style="width: 80%">
						<div class='cardbody'>
							<p class="card-text date">Date: <?= $archive['date'] ?></p>
							<p class="card-text">Poids:<?= $archive['poids'] ?></p>
							<p class="card-text">IMC: <?= $archive['IMC'] ?></p>
							<p class="card-text">Corpulence: <?= $archive['corpulence'] ?></p>
						</div>
					</div>

				<?php }

				?>
			</div>

		</section>


		<a class="btnfillred graphique" href="graphique.php">Voir votre courbe d'évolution</a>
		<div class="imgimc">
			<img src="pictures/nutrition.webp" alt="food">
			<button class="pub">
				Je veux suivre un programme personnalisé <br>
				<img src="pictures/etoile.png" alt="etoile">
				<img src="pictures/etoile.png" alt="etoile">
				<img src="pictures/etoile.png" alt="etoile"><br>
				cliquez ici
			</button>
		</div>
	<?php

} else {
	header('location: login.php');
} ?>

	<footer>
		<div class="mentions">
			<p> MY life My diet
				Immatriculation:78990008787
				contact: mylifemydiet@free.fr
				7 Rue des amazones
				75809 Paris</p>
		</div>
		<img src="pictures/facebook.png" alt="fb">
		<img src="pictures/instagram.png" alt="insta">
	</footer>
	<script src="script.js"></script>
	</body>

	</html>