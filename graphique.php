<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="style.css" rel="stylesheet">
    <title>Document</title>
</head>

<body class="bodygraph" style="background:red">


    <?php
    include('function.php');
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    session_start();
    if (isset($_SESSION['username']) && isset($_SESSION['psw'])) {

        $user = checkAdmin($_SESSION['username']);
        $archives = archives($_SESSION['username']); ?>

        <script src="https://www.gstatic.com/charts/loader.js"></script>
        <script type="text/javascript">
            google.charts.load('current', {
                packages: ['corechart', 'line']
            });
            google.charts.setOnLoadCallback(drawBasic);

            function drawBasic() {
                let data = new google.visualization.DataTable();
                data.addColumn('string', 'date');
                data.addColumn('number', 'IMC');
                <?php
                foreach ($archives as $archive) {
                ?>
                    data.addRows([
                        ["<?= $archive['date'] ?>", <?= $archive['IMC'] ?>],
                    ])
                <?php } ?>
                var options = {
                    hAxis: {
                        title: 'date'
                    },
                    vAxis: {
                        title: 'IMC'
                    }
                };

                var chart = new google.visualization.LineChart(document.getElementById('chart_div'));

                chart.draw(data, options);
            }
        </script>
    <?php } ?>

    <h1> Evolution de votre indice de masse corporelle</h1>
    <div class='chart' id="chart_div">
    </div>
    <a class="btnred" href="profil.php">Retour à votre profil</a>
</body>

</html>