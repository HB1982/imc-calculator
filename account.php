<?php
include('function.php');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="style.css" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>

    <link href="https://fonts.googleapis.com/css2?family=Luxurious+Roman&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulaire d'identification</title>
</head>


<body>
    <section class='d-flex container account'>
        <div class='left'>
            <h2>Créer un compte</h2>
            <form class="column" id="centrer" method="post">
                <p>Votre email :</p> <input class="form-control" type="text" name="mail" required>
                <p>Votre mot de passe :</p> <input class="form-control" type="password" name="psw" required>
                <p>Votre nom :</p> <input class="form-control" type="text" name="nom" required>
                <p>Votre prénom :</p> <input class="form-control" type="text" name="prenom" required>
                <p>Votre sexe :</p><select class="form-control" id="exampleFormControlSelect1" name="sex" required>
                    <option value="">Faites votre choix </option>
                    <option value="femme"> femme</option>
                    <option value="homme">homme</option>


                </select>
                <p>Votre date de naissance:</p> <input class="form-control" type="date" name="date" required>
                <p><input class="btnred" type="submit" value="Créer un compte"></p>
            </form>
        </div>
        <div class='right'>
            <img src="pictures/ll.png" alt="pommes">
        </div>
    </section>
</body>

</html>
<?php

if (isset($_POST['mail'], $_POST['psw'], $_POST['nom'], $_POST['prenom'], $_POST['sex'], $_POST['date'])) {
    $today = date("d.m.Y");
    var_dump($today);
    var_dump($_POST['date']);
    echo $_POST['mail'];
    addUser($_POST['nom'], $_POST['prenom'], $_POST['sex'], $_POST['mail'], $_POST['psw'], $_POST['date'], $today);
    header("location:login.php");
}
