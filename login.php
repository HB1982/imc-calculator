<?php


include('data.php');

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="style.css" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css2?family=Luxurious+Roman&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulaire d'identification</title>
</head>


<body>
    <section class='container login'>
        <div class='picture'>

        </div>
        <div class='d-flex bloclogin'>
            <div class='inscription'>
                <p>Pas encore de compte ?</p>
                <a class="btnwhite" href="account.php">M'inscrire</a>
                <div class='white'>
                    <img src="pictures/logo.png" alt="logo">
                </div>
            </div>
            <div class="form">
                <h2>CONNECTEZ VOUS</h2>
                <form class="column" id="centrer" action="checklogin.php" method="post">
                    <p>Email ou nom d'utilisateur :</p> <input class="form-control" type="text" name="email" required>
                    <p>Votre mot de passe :</p> <input class="form-control" type="password" name="psw" required>
                    <input class="btnred" type="submit" value="Connexion">
                </form>
            </div>
        </div>
    </section>
</body>

</html>