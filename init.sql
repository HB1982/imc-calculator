DROP DATABASE IF EXISTS IMC ;
CREATE DATABASE IMC;
USE IMC;
GRANT ALL PRIVILEGES ON clickAndCollect.* TO 'helene'@'localhost';
-- Remplacer Bob par le nom de votre utilisateur mysql


CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(100) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `sexe` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `ddn`date,
  `date` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `IMC` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `idUser` int(11) NOT NULL,
  `mail` varchar(100) NOT NULL,
  `poids` INT NOT NULL,
  `taille` INT NOT NULL,
  `corpulence` varchar(100) NOT NULL,
   `IMC` INT NOT NULL,
   `date` VARCHAR(100) NOT NULL,
  `timestamp` INT NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `metabolisme` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `idUser` int(11) NOT NULL,
  `mail` varchar(100) NOT NULL,
  `poids` FLOAT NOT NULL,
  `taille` FLOAT NOT NULL,
  `sexe` varchar(100) NOT NULL,
   `age` INT NOT NULL,
   `calories` INT NOT NULL,
   `date` VARCHAR(100) NOT NULL,
  `timestamp` INT NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
