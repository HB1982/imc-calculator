<?php
$host = "localhost";
$db   = 'IMC';
$user = "helene";
$pass = "BEURY";


$pdo = new PDO("mysql:host=$host;dbname=$db;", $user, $pass);


function checkAdmin($email)
{

    global $pdo;
    $req = $pdo->prepare("SELECT * FROM users where email=?;");
    $req->execute([$email]);
    return $req->fetch();
}

function addUser($nom, $prenom, $sexe, $email, $password, $ddn, $date)
{

    global $pdo;
    $req = $pdo->prepare("insert into users (name,firstname,sexe,email,password,ddn,date) values (?, ?, ?, ?, ?,?,?)");
    $req->execute([$nom, $prenom, $sexe, $email, $password, $ddn, $date]);
}

function addIMC($iduser, $mail, $poids, $taille, $corpulence, $imc, $date, $timestamp)
{

    global $pdo;
    $req = $pdo->prepare("insert into IMC (idUser,mail,poids,taille, corpulence,IMC,date, timestamp) values (?, ?, ?, ?, ?,?,?,?)");
    $req->execute([$iduser, $mail, $poids, $taille, $corpulence, $imc, $date, $timestamp]);
}

function addmetabolisme($iduser, $mail, $poids, $taille, $sexe, $age, $calories, $date, $timestamp)
{

    global $pdo;
    $req = $pdo->prepare("insert into metabolisme (idUser,mail,poids,taille, sexe,age,calories,date,timestamp) values (?, ?, ?, ?, ?,?,?,?,?)");
    $req->execute([$iduser, $mail, $poids, $taille, $sexe, $age, $calories, $date, $timestamp]);
}

function archives($email)
{
    global $pdo;
    $req = $pdo->prepare("SELECT * FROM IMC where mail=? ORDER BY timestamp DESC LIMIT 4");
    $req->execute([$email]);

    return $req->fetchAll();
}

function lastConnexion($email)
{
    global $pdo;
    $req = $pdo->prepare("SELECT * FROM IMC where mail=? ORDER BY timestamp DESC LIMIT 1");
    $req->execute([$email]);

    return $req->fetch();
}
function deleteAccount($email)
{
    global $pdo;
    $req = $pdo->prepare("DELETE FROM users where email=? ; delete from IMC where mail=? ;delete from metabolisme where mail=? ");
    $req->execute([$email, $email, $email]);
    return $req->fetch();
}



function modifInformations($nom, $prenom, $sexe, $email, $password, $ddn, $id)
{

    global $pdo;
    $req = $pdo->prepare("UPDATE users SET name = ?, firstname = ?, sexe = ?,email=?,password=?,ddn=? where id=?");
    $req->execute([$nom, $prenom, $sexe, $email, $password, $ddn, $id]);
}

function changeMailImc($newmail, $iduser)
{

    global $pdo;
    $req = $pdo->prepare("UPDATE IMC SET mail=? where idUser=?");
    $req->execute([$newmail, $iduser]);
}
function changeMailcalories($newmail, $iduser)
{

    global $pdo;
    $req = $pdo->prepare("UPDATE IMC SET mail=? where idUser=?");
    $req->execute([$newmail, $iduser]);
}
